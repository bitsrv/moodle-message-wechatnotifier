<?php

function xmldb_message_wechatnotifier_install() {
    global $DB;
    $result = true;

    $provider = new stdClass();
    $provider->name  = 'wechatnotifier';
    $DB->insert_record('message_processors', $provider);
    return $result;
}
