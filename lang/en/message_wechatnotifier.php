<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'message_wechatnotifier'
 *
 * @package   message_wechatnotifier
 */

$string['pluginname'] = 'Wechat Notifier Message Processor';
$string['myfield'] = 'This is my wechatnotifier field';
$string['notifierhost'] = 'Host Name';
$string['configwechathost'] = 'The host name for notifies should be sent to!';
$string['wechat_api_key'] = 'API KEY';
$String['configwechat_api_key'] = 'A certificate for application to use this host';
$string['notify_type'] = 'Notify Type';
$string['confignotify_type'] = 'The type for notifies which sent by this application';
