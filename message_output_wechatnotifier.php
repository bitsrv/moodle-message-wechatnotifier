<?php
/**
 * wechat message processor
 */
require_once($CFG->dirroot.'/message/output/lib.php');

class message_output_wechatnotifier extends message_output {
    /**
     * Processes the message
     * @param object $eventdata the event data submitted by the message sender plus $eventdata->savedmessageid
     * @return true
     */
    function send_message($eventdata) {
        global $CFG;

        if ($eventdata->userto->suspended or $eventdata->userto->deleted) {
            return true;
        }

        if (PHPUNIT_TEST) {
            // No connection to external servers allowed in phpunit tests.
            return true;
        }

        // Delete attributes that may content private information.
        if (!empty($eventdata->userfrom)) {
            $extra['userfromfullname'] = fullname($eventdata->userfrom);
        }

        // Clean HTML, push notifications must arrive clean.
        if (!empty($eventdata->smallmessage)) {
            $extra['smallmessage'] = clean_param($eventdata->smallmessage, PARAM_NOTAGS);
        }
        if (!empty($eventdata->fullmessage)) {
            $extra['fullmessage'] = clean_param($eventdata->fullmessage, PARAM_NOTAGS);
        }
        if (!empty($eventdata->fullmessagehtml)) {
            $extra['fullmessagehtml'] = clean_param($eventdata->fullmessagehtml, PARAM_NOTAGS);
        }

        if ($eventdata->component == "mod_forum"){
            $textemail_flag = "---------------------------------------------------------------------";
            $email_start_pos = strpos($extra['fullmessage'],$textemail_flag)+69;
            $email_end_pos = strrpos($extra['fullmessage'],$textemail_flag);
            $length = 1600>($email_end_pos-$email_start_pos)?($email_end_pos-$email_start_pos):1600;
            $substr_1 = mb_substr($extra['fullmessage'], $email_start_pos);
            $message_topost = $this->truncate($substr_1,$length,$length==1600?"...":"");
            $wechatmessage = $extra['smallmessage'];
            $wechatmessage .= "\n正文预览:\n".$message_topost;
        } else{
            $fullmessagelength = strlen($extra['fullmessage']);
            $length = 1600>$fullmessagelength?$fullmessagelength:1600;
            $message_topost = $this->truncate($extra['fullmessage'],$length,$length==1600?"...":"");

            $wechatmessage = $extra['userfromfullname'].': '.$message_topost."\n";
        }

        if (!empty($eventdata->contexturl)) {
            $wechatmessage .= "\n".'<a href="'.$eventdata->contexturl.'">点击查看原网页</a>';
        }

        $wechatmessage .= "\n(".get_string('noreply','message').')';

        $url = $CFG->wechat_notifierhost;
        $params['type'] = $CFG->wechat_notify_type;
        $params['api_key'] = $CFG->wechat_api_key;
        $params['badgenumber'] = $eventdata->userto->username;
        $params['message'] = $wechatmessage;
//        if ($eventdata->name == 'instantmessage') {
//            $params['message'] = $extra['smallmessage'];
//        }

        $this->request_post($url,$params);
        return true;
    }

    /**
     * Creates necessary fields in the messaging config form.
     * @param object $mform preferences form class
     */
    function config_form($preferences) {
    }

    /**
     * Parses the form submitted data and saves it into preferences array.
     * @param object $mform preferences form class
     * @param array $preferences preferences array
     */
    function process_form($form, &$preferences) {
    }

    /**
     * Loads the config data from database to put on the form (initial load)
     * @param array $preferences preferences array
     * @param int $userid the user id
     */
    function load_data(&$preferences, $userid) {
    }

    /**
     * 模拟post进行url请求
     * @param string $url
     * @param array $post_data
     */
    function request_post($url = '', $post_data = array()) {
        if (empty($url) || empty($post_data)) {
            return false;
        }
        
        $o = "";
        foreach ( $post_data as $k => $v ) 
        { 
            $o.= "$k=" . urlencode( $v ). "&" ;
        }
        $post_data = substr($o,0,-1);

        $postUrl = $url;
        $curlPost = $post_data;
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL,$postUrl);//抓取指定网页
        curl_setopt($ch, CURLOPT_HEADER, 0);//设置header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, 1);//post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = curl_exec($ch);//运行curl
        curl_close($ch);
        
        return $data;
    }

    /**
     * 截取全角和半角（汉字和英文）混合的字符串以避免乱码
     * @param string $input 字符串
     * @param int $length 截取的字节长度
     * @param string $suffix 后缀
     */
    function truncate($input, $length, $suffix = '...')
    {
        if(strlen($input) <= $length){
            return $input;
        }
        $i = 0;
        $sb = array();
        while($i < $length - strlen($suffix)){
            $ord = ord(substr($input, $i, 1));
            $real_char_length = 1;
            if($ord >= 224){
                $real_char_length = 3;
            }elseif($ord >= 192){
                $real_char_length = 2;
            }else{
                $real_char_length = 1;
            }
            $sb[] = substr($input, $i, $real_char_length);
            $i += $real_char_length;
        }
        return implode('', $sb) . $suffix;
    }
}
