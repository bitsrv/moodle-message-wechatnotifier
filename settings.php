<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * wechat notifier configuration page
 *
 * @package    message_wechatnotifier
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    $settings->add(new admin_setting_configtext('wechat_notifierhost', get_string('notifierhost', 'message_wechatnotifier'), get_string('configwechathost', 'message_wechatnotifier'), 'localhost', PARAM_RAW));
    $settings->add(new admin_setting_configtext('wechat_api_key', get_string('wechat_api_key', 'message_wechatnotifier'), get_string('configwechat_api_key', 'message_wechatnotifier'), 'my-api-key', PARAM_RAW));
    $settings->add(new admin_setting_configtext('wechat_notify_type', get_string('notify_type', 'message_wechatnotifier'), get_string('confignotify_type', 'message_wechatnotifier'), 'notify type', PARAM_RAW));
}
